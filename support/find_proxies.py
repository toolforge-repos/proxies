import toolforge
import pymysql
import yaml
import simplejson as json
import os
import requests
import re
config = yaml.safe_load(open(os.path.join(os.path.dirname(__file__), '..', 'src', 'config.yaml')))

def connect():
    return pymysql.connect(
        database=config['DB_NAME'],
        host='tools.db.svc.eqiad.wmflabs',
        read_default_file=os.path.expanduser("~/replica.my.cnf"),
        charset='utf8mb4',
    )


if __name__ == "__main__":
    conn = connect()
    enwiki = toolforge.connect('enwiki', cluster='analytics')
    with enwiki.cursor() as cur:
        cur.execute('select ipb_address from ipblocks where ipb_by=8760229 and ipb_address not in (select gb_address from centralauth_p.globalblocks);')
        data = cur.fetchall()
    for row in data:
        with conn.cursor() as cur:
            cur.execute('insert into ips (ip_ip) values (%s)', row[0].decode('utf-8'))
            conn.commit()

