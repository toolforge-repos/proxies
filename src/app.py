# -*- coding: utf-8 -*-
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import flask
import os
import yaml
import simplejson as json
import requests
from urllib.parse import quote
from flask import redirect, request, jsonify, make_response, render_template
import mwoauth
import mwparserfromhell
from requests_oauthlib import OAuth1
import random
import toolforge
from email.mime.text import MIMEText

app = flask.Flask(__name__)
application = app

# Load configuration from YAML file
__dir__ = os.path.dirname(__file__)
app.config.update(
    yaml.safe_load(open(os.path.join(__dir__, 'config.yaml'))))

requests.utils.default_user_agent = lambda: app.config['USER_AGENT']

key = app.config['CONSUMER_KEY']
secret = app.config['CONSUMER_SECRET']

@app.route('/')
def index():
    limit = int(request.args.get('limit', "50"))
    page = int(request.args.get('page', "0"))
    offset = page*limit
    conn = toolforge.connect('enwiki')
    with conn.cursor() as cur:
        cur.execute('select ipb_address from ipblocks where ipb_by=8760229 and ipb_address not in (select gb_address from centralauth_p.globalblocks) limit %s, %s', (offset, limit))
        data = cur.fetchall()
        ips = []
        for row in data:
            ips.append(row[0].decode('utf-8'))
        next = cur.rowcount == 50
        previous = page > 0
    return render_template('index.html', ips=ips, limit=limit, page=page, next=next, previous=previous)

if __name__ == "__main__":
	app.run(debug=True, threaded=True)
